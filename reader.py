import os
import argparse
import feedparser
from dateutil import parser, tz
from datetime import datetime, timedelta, date
from bs4 import BeautifulSoup
import requests
from datetime import datetime as dt
import pytz
import html
from markdownify import markdownify as md
from dateutil.tz import gettz


def gen_tzinfos():
    for zone in pytz.common_timezones:
        try:
            tzdate = pytz.timezone(zone).localize(dt.utcnow(), is_dst=None)
        except pytz.NonExistentTimeError:
            pass
        else:
            tzinfo = gettz(zone)

            if tzinfo:
                yield tzdate.tzname(), str(tzinfo)


TZINFOS = dict(gen_tzinfos())
TZINFOS['EDT'] = "tzfile('/usr/share/zoneinfo/US/Eastern')"


def read(feed_url):
    news_feed = feedparser.parse(feed_url)
    today = parser.parse(str(datetime.now(tz.tzlocal())))
    delta = timedelta(hours=-24)
    ret = []
    count = 0
    for entry in news_feed.entries:
        if hasattr(entry, 'published'):
            d = parser.parse(entry.published, tzinfos=TZINFOS)
        elif hasattr(entry, 'updated'):
            d = parser.parse(entry.updated, tzinfos=TZINFOS)
        else:
            continue

        if ((today + delta).timestamp() > d.timestamp()) or d > today:
            break
        count += 1
        link = None
        count += 1
        if "geone.ws" in entry.link:
            link = extract_actual_url(entry)
        if not link:
            link = entry.link
        if 'camptocamp_fossgis' not in link:
            title = md(entry.title.replace("#", ""))
            ret.append(f"+ [{title}]({link})")

    if count:
        print(f"processed {feed_url} for {count-1} items")
    return ret


def extract_actual_url(entry):
    r = requests.get(entry.link)
    soup = BeautifulSoup(r.text, features="html5lib")

    if soup:
        div = soup.find('div', class_='entry-content')
    link = None
    if div:
        link = div.find("a")["href"]
    return link


def main():

    parser = argparse.ArgumentParser(prog='scanner')
    parser.add_argument("-t",
                        "--test",
                        dest="test",
                        help="do a test run",
                        action="store_true",
                        required=False)
    options = parser.parse_args()

    if not options.test:
        update_log()
    else:
        dir = os.path.dirname(__file__)
        feeds = os.path.join(dir, "feeds.txt")
        with open(feeds) as f:
            for line in f:
                if not line.startswith("#"):
                    res = read(line)
                    for r in res:
                        print(r)
                        items = get_contents()
                        for item in items:
                            print(item)


def update_log():
    today = parser.parse(str(datetime.now(tz.tzlocal())))
    file = f"/home/ian/Documents/notes/journal/{date.strftime(today, '%Y-%m-%d')}.md"
    contents = []
    empty = False
    try:
        with open(file, 'r') as f:
            contents = f.readlines()
    except FileNotFoundError:
        contents.append(f"# Codelog {date.strftime(today, '%A %b %-d, %Y')}\n")
        empty = True

    line_no = 1
    contents.insert(line_no, "\n## RSS Items\n\n")
    line_no += 1
    items = get_contents()
    for item in items:
        contents.insert(line_no, item+"\n")
        line_no += 1
    contents.insert(line_no, "\n")
    with open(file, "w") as f:
        contents = "".join(contents)
        f.write(contents)
        if empty:
            f.write(
                f"## Plan\n\n[[{date.strftime(today-timedelta(days = 1), '%Y-%m-%d')}]] "
                + f"[[{date.strftime(today+timedelta(days = 1), '%Y-%m-%d')}]]\n")


def get_contents():
    contents = []
    dir = os.path.dirname(__file__)
    feeds = os.path.join(dir, "feeds.txt")
    with open(feeds) as f:
        for line in f:
            res = read(line)
            for r in res:
                line = html.unescape(r)
                contents.append(line)

    return contents


if __name__ == '__main__':
    main()
