## Steps
1. Build a training set of interesting and uninteresting blog posts.
2. Run the baysian classifier on the training data.
3. use the trained classifier on rss feeds daily to alert me to interesting blogs.

### Step 1

Write a script to allow viewing of RSS feeds and a set of buttons for scoring blog entries.

See this [example](https://www.blog.pythonlibrary.org/2014/01/09/wxpython-create-rss-reader/) on displaying rss with wxPython 

    pip install -U \
    -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/ubuntu-16.04 \
    wxPython
    pip install unidecode
    pip install ObjectListView
    
Makes a nice reader